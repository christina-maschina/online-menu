package kristina.topia.online_menu.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@Setter
@Getter
public class IngredientDto implements Serializable {
    private Long id;
    private String name;
}

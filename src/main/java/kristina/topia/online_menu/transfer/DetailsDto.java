package kristina.topia.online_menu.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@Getter
@Setter
public class DetailsDto implements Serializable {

    private List<OrderDto> orders = new ArrayList<>();
    private BigDecimal totalPrice;
    private Double deliveryPrice;
    private Double totalPriceWithDelivery;

}

package kristina.topia.online_menu.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class ResponseDto {

    private Object responseBody;
    private ResultStatusDto status;
}

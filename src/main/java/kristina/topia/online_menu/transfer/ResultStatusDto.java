package kristina.topia.online_menu.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@Getter
@Setter
public class ResultStatusDto {

    private boolean success;
    private String message;

    public ResultStatusDto(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
}

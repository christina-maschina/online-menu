package kristina.topia.online_menu.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@Data
public class DishDto implements Serializable {

    private Long id;
    private String name;
    private BigDecimal price;

    private Set<Long> ingredientsIDs = new HashSet<>();
    private Set<IngredientDto> ingredientDtos = new HashSet<>();

}

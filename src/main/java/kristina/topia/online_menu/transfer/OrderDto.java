package kristina.topia.online_menu.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@Getter
@Setter
public class OrderDto {

    private Long id;
    private DishDto dishDto;
    private Long amount;
    private BigDecimal entityPrice;

}


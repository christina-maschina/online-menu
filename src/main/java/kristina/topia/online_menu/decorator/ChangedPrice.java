package kristina.topia.online_menu.decorator;

public class ChangedPrice extends DeliveryPriceDecorator{

    public ChangedPrice(DeliveryPrice deliveryPrice) {

        super(deliveryPrice);
    }

    @Override
    public double count(double sumOfPrices) {

        return sumOfPrices * 0.1;
    }
}

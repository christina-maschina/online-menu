package kristina.topia.online_menu.decorator;

public abstract class DeliveryPriceDecorator implements DeliveryPrice{
    private DeliveryPrice deliveryPrice;

    public DeliveryPriceDecorator(DeliveryPrice deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public DeliveryPrice getDeliveryPrice() {
        return this.deliveryPrice;
    }
}

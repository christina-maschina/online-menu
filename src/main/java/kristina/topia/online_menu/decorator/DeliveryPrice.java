package kristina.topia.online_menu.decorator;

public interface DeliveryPrice {

    public abstract double count(double price);
}

package kristina.topia.online_menu.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class NotFoundException extends RuntimeException {
    private long id;

    public NotFoundException(long id) {
        this.id = id;
    }

    public boolean isStatus() {
        return false;
    }

    public String getMessage() {
        return "The item with ID " + id + " doesn't exists";
    }

    public HttpStatus getResponseHttpStatus() { return HttpStatus.NOT_FOUND; }
}

package kristina.topia.online_menu.exception;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends RuntimeException {

    public HttpStatus getResponseHttpStatus() {
        return HttpStatus.FORBIDDEN;
    }

    public boolean isStatus() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Violation of UNIQUE KEY constraint";
    }
}

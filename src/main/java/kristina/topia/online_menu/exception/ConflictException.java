package kristina.topia.online_menu.exception;

import org.springframework.http.HttpStatus;

public class ConflictException extends RuntimeException {

    public HttpStatus getResponseHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    public boolean isSuccess() {
        return false;
    }

    @Override
    public String getMessage() {
        return "The DELETE statement conflicted with the REFERENCE constraint";
    }
}

package kristina.topia.online_menu.controller;

import kristina.topia.online_menu.exception.ForbiddenException;
import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.service.DishService;
import kristina.topia.online_menu.transfer.DishDto;
import kristina.topia.online_menu.transfer.ResponseDto;
import kristina.topia.online_menu.transfer.ResultStatusDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("dish")
public class DishController implements Serializable {

    @Autowired
    DishService dishService;

    @PostMapping("/add")
    public ResponseEntity<ResponseDto> addDish(@RequestBody DishDto dishDto) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The dish was successfully added");

        try {
            dishService.addDish(dishDto);

        } catch (NotFoundException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        } catch (ForbiddenException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());

        }
        response.setStatus(responseStatus);

        return new ResponseEntity<>(response, responseHttpStatus);

    }

    @GetMapping("/get")
    public ResponseEntity<ResponseDto> getAll() {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The dishes were successfully displayed");

        List<DishDto> responseBody = dishService.getAll();

        if (ObjectUtils.isEmpty(responseBody)) {
            responseHttpStatus = HttpStatus.NO_CONTENT;
            responseStatus.setSuccess(false);
            responseStatus.setMessage("There is no content");
        }
        response.setStatus(responseStatus);
        response.setResponseBody(responseBody);

        return new ResponseEntity<>(response, responseHttpStatus);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<ResponseDto> getById(@PathVariable long id) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The dish was successfully displayed");
        DishDto responseBody = new DishDto();

        try {
            responseBody = dishService.getById(id);
            response.setResponseBody(responseBody);
        } catch (NotFoundException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        }

        response.setStatus(responseStatus);

        return new ResponseEntity<>(response, responseHttpStatus);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ResponseDto> updateDish(@RequestBody DishDto dishDto, @PathVariable long id) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The dish was successfully updated");

        try {
            dishService.updateDish(id, dishDto);
        } catch (NotFoundException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        } catch (ForbiddenException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        }
        response.setStatus(responseStatus);
        return new ResponseEntity<>(response, responseHttpStatus);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ResponseDto> deleteDish(@PathVariable long id) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The dish was successfully deleted");

        try {
            dishService.deleteDish(id);
        } catch (NotFoundException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        }

        response.setStatus(responseStatus);

        return new ResponseEntity<>(response, responseHttpStatus);
    }
}

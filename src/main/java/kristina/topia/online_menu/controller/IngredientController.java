package kristina.topia.online_menu.controller;

import kristina.topia.online_menu.exception.ConflictException;
import kristina.topia.online_menu.exception.ForbiddenException;
import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.service.IngredientService;
import kristina.topia.online_menu.transfer.IngredientDto;
import kristina.topia.online_menu.transfer.ResponseDto;
import kristina.topia.online_menu.transfer.ResultStatusDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("ingredient")
@Setter
@Getter
public class IngredientController implements Serializable {

    @Autowired
    private IngredientService ingredientService;

    @PostMapping("/add")
    public ResponseEntity<ResponseDto> addIngredient(@RequestBody IngredientDto ingredientDto) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The ingredient was successfully added");

        try {
            ingredientService.addIngredient(ingredientDto);
        } catch (ForbiddenException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        }
        response.setStatus(responseStatus);

        return new ResponseEntity<>(response, responseHttpStatus);
    }

    @GetMapping("/get")
    public ResponseEntity<ResponseDto> getAll() {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The ingredients were successfully displayed");

        List<IngredientDto> responseBody = ingredientService.getAll();
        if (ObjectUtils.isEmpty(responseBody)) {
            responseHttpStatus = HttpStatus.NO_CONTENT;
            responseStatus.setSuccess(false);
            responseStatus.setMessage("There is no content");
        }

        response.setStatus(responseStatus);
        response.setResponseBody(responseBody);

        return new ResponseEntity<>(response, responseHttpStatus);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<ResponseDto> getById(@PathVariable long id) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The ingredient was successfully displayed");
        IngredientDto responseBody = new IngredientDto();

        try {
            responseBody = ingredientService.getById(id);
            response.setResponseBody(responseBody);
        } catch (NotFoundException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        }
        response.setStatus(responseStatus);

        return new ResponseEntity<>(response, responseHttpStatus);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ResponseDto> updateIngredient(@RequestBody IngredientDto ingredientDto, @PathVariable long id) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The ingredient was successfully updated");

        try {
            ingredientService.updateIngredient(ingredientDto, id);
        } catch (NotFoundException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        } catch (ForbiddenException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        }

        response.setStatus(responseStatus);

        return new ResponseEntity<>(response, responseHttpStatus);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ResponseDto> deleteIngredient(@PathVariable long id) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The ingredient was successfully deleted");

        try {
            ingredientService.deleteIngredient(id);
        } catch (NotFoundException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        } catch (ConflictException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isSuccess());
            responseStatus.setMessage(ex.getMessage());
        }

        response.setStatus(responseStatus);

        return new ResponseEntity<>(response, responseHttpStatus);
    }

}

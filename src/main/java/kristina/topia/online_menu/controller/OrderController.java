package kristina.topia.online_menu.controller;

import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.service.OrderService;
import kristina.topia.online_menu.transfer.DetailsDto;
import kristina.topia.online_menu.transfer.OrderDto;
import kristina.topia.online_menu.transfer.ResponseDto;
import kristina.topia.online_menu.transfer.ResultStatusDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController implements Serializable {
    @Autowired
    private OrderService orderService;

    @GetMapping("/get")
    public ResponseEntity<ResponseDto> getOrder(@RequestBody List<OrderDto> orderDtos) {
        ResponseDto response = new ResponseDto();

        HttpStatus responseHttpStatus = HttpStatus.OK;
        ResultStatusDto responseStatus = new ResultStatusDto(true, "The order was successfully displayed");

        DetailsDto responseBody = new DetailsDto();
        try {
            responseBody = orderService.getOrder(orderDtos);
            response.setResponseBody(responseBody);
        } catch (NotFoundException ex) {
            responseHttpStatus = ex.getResponseHttpStatus();
            responseStatus.setSuccess(ex.isStatus());
            responseStatus.setMessage(ex.getMessage());
        }
        response.setStatus(responseStatus);

        return new ResponseEntity<>(response, responseHttpStatus);

    }

}

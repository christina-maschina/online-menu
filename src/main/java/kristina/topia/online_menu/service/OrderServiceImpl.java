package kristina.topia.online_menu.service;

import kristina.topia.online_menu.decorator.ChangedPrice;
import kristina.topia.online_menu.decorator.ConstantPrice;
import kristina.topia.online_menu.decorator.DeliveryPrice;
import kristina.topia.online_menu.domain.Dish;
import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.repository.DishRepository;
import kristina.topia.online_menu.transfer.DetailsDto;
import kristina.topia.online_menu.transfer.DishDto;
import kristina.topia.online_menu.transfer.OrderDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.*;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private DishRepository dishRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public DetailsDto getOrder(List<OrderDto> orderDtos) throws NotFoundException {

        double sumOfPrices = 0.0;
        DetailsDto detailsDto = new DetailsDto();

        for (OrderDto order : orderDtos) {
            Dish dish = dishRepository.findById(order.getId()).orElse(null);
            if (ObjectUtils.isEmpty(dish)) {
                throw new NotFoundException(order.getId());
            }

            DishDto dishDto = modelMapper.map(dish, DishDto.class);
            OrderDto orderDto = new OrderDto();

            orderDto.setDishDto(dishDto);
            orderDto.setAmount(order.getAmount());
            orderDto.setEntityPrice(dishDto.getPrice().multiply(BigDecimal.valueOf(order.getAmount())));
            detailsDto.getOrders().add(orderDto);

            sumOfPrices += orderDto.getEntityPrice().doubleValue();

        }
        detailsDto.setTotalPrice(BigDecimal.valueOf(sumOfPrices));

        /*implementacija konstantne cijene
        DeliveryPrice deliveryPrice=new ConstantPrice();
        detailsDto.setDeliveryPrice(deliveryPrice.count(10.0));*/

        //implementacija koja za cijenu dostave vraca 10% cijene narudzbe
        DeliveryPrice deliveryPrice = new ChangedPrice(new ConstantPrice());
        detailsDto.setDeliveryPrice(deliveryPrice.count(sumOfPrices));

        detailsDto.setTotalPriceWithDelivery(sumOfPrices + detailsDto.getDeliveryPrice());

        return detailsDto;
    }
}

package kristina.topia.online_menu.service;

import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.transfer.DetailsDto;
import kristina.topia.online_menu.transfer.OrderDto;

import java.util.List;

public interface OrderService {
    DetailsDto getOrder(List<OrderDto> orderDtos) throws NotFoundException;
}

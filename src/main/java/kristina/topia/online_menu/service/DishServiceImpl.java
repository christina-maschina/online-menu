package kristina.topia.online_menu.service;

import kristina.topia.online_menu.domain.Dish;
import kristina.topia.online_menu.domain.Ingredient;
import kristina.topia.online_menu.exception.ForbiddenException;
import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.repository.DishRepository;
import kristina.topia.online_menu.repository.IngredientRepository;
import kristina.topia.online_menu.transfer.DishDto;
import kristina.topia.online_menu.transfer.IngredientDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class DishServiceImpl implements DishService {

    @Autowired
    private DishRepository dishRepository;
    @Autowired
    private IngredientRepository ingredientRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void addDish(DishDto dishDto) throws NotFoundException, ForbiddenException {

        List<Dish> dishes = (List<Dish>) dishRepository.findAll();
        Optional<Dish> optional = dishes.stream().filter(dish -> dish.getName().equals(dishDto.getName())).findFirst();
        if (optional.isPresent()) {
            throw new ForbiddenException();
        }

        Set<Ingredient> ingredients = new HashSet<>();
        if (!ObjectUtils.isEmpty(dishDto.getIngredientsIDs())) {
            dishDto.getIngredientsIDs()
                    .forEach(id -> {
                                Ingredient ingredient = ingredientRepository.findById(id).orElse(null);
                                if (ObjectUtils.isEmpty(ingredient)) {
                                    throw new NotFoundException(id);
                                }
                                ingredients.add(ingredient);
                            }
                    );
        }
        Dish dish = modelMapper.map(dishDto, Dish.class);
        dish.setIngredients(ingredients);

        dishRepository.save(dish);

    }

    @Override
    public List<DishDto> getAll() {

        List<Dish> dishes = (List<Dish>) dishRepository.findAll();
        Set<IngredientDto> ingredientDtos = new HashSet<>();

        List<DishDto> dishDtos = dishes
                .stream()
                .map(dish -> {
                    Type ingredientType = new TypeToken<Set<IngredientDto>>() {
                    }.getType();
                    Set<IngredientDto> dishDtoTypeSet = modelMapper.map(dish.getIngredients(), ingredientType);

                    DishDto dishDto = modelMapper.map(dish, DishDto.class);
                    dishDto.setIngredientDtos(dishDtoTypeSet);

                    return dishDto;
                })
                .collect(Collectors.toList());

        return dishDtos;
    }

    @Override
    public DishDto getById(long id) throws NotFoundException {
        Dish dish = dishRepository.findById(id).orElse(null);

        if (ObjectUtils.isEmpty(dish)) {
            throw new NotFoundException(id);
        }

        Type ingredientType = new TypeToken<Set<IngredientDto>>() {
        }.getType();
        Set<IngredientDto> dishDtoTypeSet = modelMapper.map(dish.getIngredients(), ingredientType);

        DishDto dishDto = modelMapper.map(dish, DishDto.class);
        dishDto.setIngredientDtos(dishDtoTypeSet);

        return dishDto;
    }

    @Override
    public void updateDish(Long id, DishDto dishDto) throws NotFoundException, ForbiddenException {
        Dish dish = dishRepository.findById(id).orElse(null);

        if (ObjectUtils.isEmpty(dish)) {
            throw new NotFoundException(id);
        }

        if (dishDto.getName() != null && !dishDto.getName().equals(dish.getName())) {
            List<Dish> dishes = (List<Dish>) dishRepository.findAll();
            Optional<Dish> optional = dishes
                    .stream()
                    .filter(dish1 -> dish1.getName().equals(dishDto.getName()))
                    .findFirst();
            if (optional.isPresent()) {
                throw new ForbiddenException();
            }
        }
        mapping(dishDto, dish);

    }

    @Override
    public void deleteDish(long id) throws NotFoundException {
        Dish dish = dishRepository.findById(id).orElse(null);

        if (ObjectUtils.isEmpty(dish)) {
            throw new NotFoundException(id);
        }
        dishRepository.deleteById(id);
    }


    private void mapping(DishDto dishDto, Dish dish) {
        Set<Ingredient> ingredients = new HashSet<>();
        Set<Long> ingredientsIds = dishDto.getIngredientsIDs();

        if (!ObjectUtils.isEmpty(ingredientsIds)) {
            ingredientsIds.forEach(id2 -> {
                Ingredient ingredient = ingredientRepository.findById(id2).orElse(null);
                if (ObjectUtils.isEmpty(ingredient)) {
                    throw new NotFoundException(id2);
                }
                ingredients.add(ingredient);

            });
        }

        dishDto.setId(dish.getId());
        modelMapper.map(dishDto, dish);
        dish.setIngredients(ingredients);

        dishRepository.save(dish);
    }

}

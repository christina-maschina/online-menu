package kristina.topia.online_menu.service;

import kristina.topia.online_menu.exception.ForbiddenException;
import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.transfer.DishDto;

import java.util.List;

public interface DishService {

    void addDish(DishDto dishDto) throws NotFoundException, ForbiddenException;

    List<DishDto> getAll();

    DishDto getById(long id) throws NotFoundException;

    void updateDish(Long id, DishDto dishDto) throws NotFoundException, ForbiddenException;

    void deleteDish(long id) throws NotFoundException;
}

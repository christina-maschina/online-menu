package kristina.topia.online_menu.service;

import kristina.topia.online_menu.exception.ConflictException;
import kristina.topia.online_menu.exception.ForbiddenException;
import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.transfer.IngredientDto;

import java.util.List;

public interface IngredientService {

    void addIngredient(IngredientDto ingredientDto) throws ForbiddenException;

    List<IngredientDto> getAll();

    IngredientDto getById(long id) throws NotFoundException;

    void updateIngredient(IngredientDto ingredientDto, long id) throws NotFoundException, ForbiddenException;

    void deleteIngredient(long id) throws NotFoundException, ConflictException;
}

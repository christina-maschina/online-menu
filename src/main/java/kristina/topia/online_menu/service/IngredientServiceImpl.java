package kristina.topia.online_menu.service;

import kristina.topia.online_menu.domain.Ingredient;
import kristina.topia.online_menu.exception.ConflictException;
import kristina.topia.online_menu.exception.ForbiddenException;
import kristina.topia.online_menu.exception.NotFoundException;
import kristina.topia.online_menu.repository.DishRepository;
import kristina.topia.online_menu.repository.IngredientRepository;
import kristina.topia.online_menu.transfer.IngredientDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IngredientServiceImpl implements IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;
    @Autowired
    private DishRepository dishRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void addIngredient(IngredientDto ingredientDto) throws ForbiddenException {
        List<Ingredient> ingredients = (List<Ingredient>) ingredientRepository.findAll();

        Optional<Ingredient> optional = ingredients.stream().filter(ingredient -> ingredient.getName().equals(ingredientDto.getName())).findFirst();
        if (optional.isPresent()) {
            throw new ForbiddenException();
        }
        Ingredient ingredient = modelMapper.map(ingredientDto, Ingredient.class);
        ingredientRepository.save(ingredient);
    }

    @Override
    public List<IngredientDto> getAll() {
        List<Ingredient> ingredients = (List<Ingredient>) ingredientRepository.findAll();

        List<IngredientDto> ingredientDtos = ingredients
                .stream()
                .map(ingredient -> modelMapper.map(ingredient, IngredientDto.class))
                .collect(Collectors.toList());

        return ingredientDtos;
    }

    @Override
    public IngredientDto getById(long id) throws ForbiddenException {
        Ingredient ingredient = ingredientRepository.findById(id).orElse(null);

        if (ObjectUtils.isEmpty(ingredient)) {
            throw new NotFoundException(id);
        }
        IngredientDto ingredientDto = modelMapper.map(ingredient, IngredientDto.class);

        return ingredientDto;
    }

    @Override
    public void updateIngredient(IngredientDto ingredientDto, long id) throws NotFoundException, ForbiddenException {

        Ingredient ingredient = ingredientRepository.findById(id).orElse(null);

        if (ObjectUtils.isEmpty(ingredient)) {
            throw new NotFoundException(id);
        }

        List<Ingredient> ingredients = (List<Ingredient>) ingredientRepository.findAll();
        Optional<Ingredient> optional = ingredients
                .stream()
                .filter(ingredient1 -> ingredient1.getName().equals(ingredientDto.getName()))
                .findFirst();
        if (optional.isPresent()) {
            throw new ForbiddenException();
        }

        ingredientDto.setId(ingredient.getId());
        modelMapper.map(ingredientDto, ingredient);

        ingredientRepository.save(ingredient);
    }

    @Override
    public void deleteIngredient(long id) throws NotFoundException, ConflictException {
        Ingredient ingredient = ingredientRepository.findById(id).orElse(null);

        if (ObjectUtils.isEmpty(ingredient)) {
            throw new NotFoundException(id);
        }

        Long ingredientIdFromJoinColumn = ingredientRepository.findIngredientIdFromJoinColumn(id);
        if (ObjectUtils.isEmpty(ingredientIdFromJoinColumn)) {
            ingredientRepository.delete(ingredient);
        } else {
            throw new ConflictException();
        }

    }
}

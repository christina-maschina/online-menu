package kristina.topia.online_menu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineMenuApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineMenuApplication.class, args);
	}

}

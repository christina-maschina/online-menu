package kristina.topia.online_menu.repository;

import kristina.topia.online_menu.domain.Ingredient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {

    @Query(value = " SELECT TOP 1 dish_ingredient.ingredient_id FROM dish_ingredient INNER JOIN" +
            "  ingredient ON dish_ingredient.ingredient_id=ingredient.id" +
            "  WHERE ingredient.id= :id", nativeQuery = true)
    Long findIngredientIdFromJoinColumn(@Param("id") long ingredientId);
}
